# Castle Constructor AI

## About

This is a project to create an AI that can manipulate the game "Bonsai Castles" with scripts and build castles that look like that.


## System requirement

 - Python 3.9.1


## License

 - MIT License


## Author

 - Twitter
   - @satofumi_
