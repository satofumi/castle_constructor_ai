import map_select


class Title:
    def __init__(self, command):
        self._command = command

    def normal_game(self):
        scene_name = self._command.scene()
        if scene_name == 'Title':
            self._command.send('Title.PlayGame')
            self._command.send('Title.NewGame')
            self._command.send('Title.NormalMode')
            self._command.wait_scene_changed('MapSelect')

        return map_select.MapSelect(self._command)
