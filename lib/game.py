class Game:
    def __init__(self, command):
        self._command = command


    def max_population(self):
        return self._command.max_population()

    def population(self):
        return self._command.population()

    def build_points(self):
        return self._command.build_point()

    def spirit_points(self):
        return self._command.spirit_point()

    def resources(self):
        return self._command.resources()


    def set_max_speed(self):
        speed_index = self._command.play_speed()
        for i in range(3 - speed_index):
            self._command.send('Game.Play')

    def skip_first_messages(self):
        self._command.wait_scene_changed('Game')
        self._command.send('Game.ConfirmPanelOk')
        self._command.send('KeyCode.Escape')

    def skip_next_build(self):
        self.enter_defense()
        self.set_max_speed()
        self.wait_defense_end()
        self.enter_gather()
        self.wait_gather_end()

    def skip_before_build(self):
        self._command.skip_before_build()

    def enter_defense(self):
        self._command.enter_defense()

    def wait_defense_end(self):
        self._command.wait_defense_end()

    def enter_gather(self):
        self._command.enter_gather()

    def wait_gather_end(self):
        self._command.wait_gather_end()


    def build(self, name, x, y, degree):
        self._command.build_building(name.value, x, y)

    def place_turret(self, name, x, y, degree):
        self._command.place_turret(name.value, x, y)

    def research(self, name):
        self._command.research_tech(name.value)


    def send(self, command):
        self._command.send(command)
