from enum import Enum

import game


class Map(Enum):
    Plane = 0

class Difficulty(Enum):
    Normal = 0


class MapSelect:
    def __init__(self, command):
        self._command = command

    def start(self, *, map_type=Map.Plane, difficulty=Difficulty.Normal, shield=None):
        self._command.wait_enable('MapSelect.InputField.MapCode')

        # !!! debug
        self._command.send('MapSelect.InputField.MapCode.4F665619')

        # !!! use argument settings
        self._command.wait_enable_and_press('MapSelect.Start')
        return game.Game(self._command)
