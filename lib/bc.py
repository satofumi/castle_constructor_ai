import sys
from enum import Enum, IntEnum

import packet
import command
import title
import map_select
import game


class Map(Enum):
    Plane = map_select.Map.Plane

class Difficulty(Enum):
    Normal = map_select.Difficulty.Normal

class Building(Enum):
    House = 'House'

class Turret(Enum):
    Bow = 'BowTurret'

class Research(Enum):
    Enemy_1 = 'Tech_Enemy_0'
    Enemy_2 = 'Tech_Enemy_1'
    Enemy_3 = 'Tech_Enemy_2'
    Enemy_4 = 'Tech_Enemy_3'
    Enemy_5 = 'Tech_Enemy_4'
    Enemy_6 = 'Tech_Enemy_5'
    Enemy_7 = 'Tech_Enemy_6'


_command = None
_packet = None


def connect():
    global _packet
    _packet = packet.Packet()
    _packet.connect()
    global _command
    _command = command.Command(_packet)

def disconnect():
    _packet.close()


def to_title_scene():
    global _command
    # !!! change title commands
    return title.Title(_command)

def game_scene():
    global _command
    return game.Game(_command)
