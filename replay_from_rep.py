import sys
import yaml


def _load_yaml(file_path):
    with open(file_path) as f:
        return yaml.safe_load(f)


def _play_round_actions(snapshot, round_value):
    if not 'Round' in snapshot:
        return False

    if snapshot['Round'] != round_value:
        return False

    for value in snapshot['TerrainHeights']:
        #print(value)
        pass

    for value in snapshot['WaterHeights']:
        pass

    for value in snapshot['RemoveBuildings']:
        pass

    for value in snapshot['AppendBuildings']:
        pass

    for value in snapshot['RemoveTurrets']:
        pass

    for value in snapshot['AppendTurrets']:
        pass

    for value in snapshot['RemoveRoads']:
        pass

    for value in snapshot['AppendRoads']:
        pass

    for value in snapshot['RemoveIshigaki']:
        pass

    for value in snapshot['AppendIshigaki']:
        pass

    for value in snapshot['RemoveTrees']:
        pass

    for value in snapshot['AppendTrees']:
        pass

    return True


def replay(data):
    # !!! get round value
    round_value = 1

    played = True
    while played:
        played = False
        for snapshot in data['Snapshots']:
            played |= _play_round_actions(snapshot, round_value)
        round_value += 1


if __name__ == '__main__':
    rep_file = sys.argv[1]
    data = _load_yaml(rep_file)
    replay(data)
