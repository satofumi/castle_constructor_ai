import sys
sys.path.append('lib')
sys.path.append('bc_script/lib')

import bc


def _build_houses(game, center, count):
    if game.population() < game.max_population():
        return count

    cx = center[0]
    cy = center[1]

    if count == 0:
        game.build(bc.Building.House, cx, cy, 0)
        count += 1

    else:
        n = int(game.build_points() / 30)
        for i in range(n):
            fixed_count = count - 1
            j = int(fixed_count / 4)
            k = fixed_count % 4
            if k == 0:
                x = cx + 2 + (j * 2)
                y = cy
            elif k == 1:
                x = cx - 2 - (j * 2)
                y = cy
            elif k == 2:
                x = cx
                y = cy - 2 - (j * 2)
            else:
                x = cx
                y = cy + 2 + (j * 2)

            game.build(bc.Building.House, x, y, 0)
            count += 1

    return count


def _place_turrets(game, center, count):
    n = int(game.spirit_points() / 10)
    cx = center[0]
    cy = center[1]

    for i in range(n):
        j = int(count / 4)
        k = count % 4
        if k == 0:
            x = cx + 2 + j
            y = cy + 2 + j
        elif k == 1:
            x = cx - 1 - j
            y = cy - 1 - j
        elif k == 2:
            x = cx + 2 + j
            y = cy - 1 - j
        else:
            x = cx - 1 - j
            y = cy + 2 + j

        game.place_turret(bc.Turret.Bow, x, y, 0)
        count += 1

    return count

def _research_enemy(game):
    techs = [
        { 'name': bc.Research.Enemy_1, 'resources': 10 },
        { 'name': bc.Research.Enemy_2, 'resources': 20 },
        { 'name': bc.Research.Enemy_3, 'resources': 50 },
        { 'name': bc.Research.Enemy_4, 'resources': 100 },
        { 'name': bc.Research.Enemy_5, 'resources': 200 },
        { 'name': bc.Research.Enemy_6, 'resources': 400 },
        { 'name': bc.Research.Enemy_7, 'resources': 100 },
    ]
    for tech in techs:
        if game.resources() < tech['resources']:
            break
        game.research(tech['name'])


def place_turret_spiral(game):
    center = [49, 49]
    house_count = 0
    turret_count = 0

    while True:
        game.skip_before_build()

        house_count = _build_houses(game, center, house_count)
        turret_count = _place_turrets(game, center, turret_count)
        _research_enemy(game)

        game.skip_next_build()


if __name__ == '__main__':
    bc.connect()

    title = bc.to_title_scene()
    map_select = title.normal_game()
    game = map_select.start(map_type=bc.Map.Plane,
                            difficulty=bc.Difficulty.Normal,
                            shield=None)
    game.skip_first_messages()

    place_turret_spiral(game)
