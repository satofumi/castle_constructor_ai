import sys
sys.path.append('lib')
sys.path.append('bc_script/lib')

import bc


def _click_research_button(game):
    game.research(bc.Research.Enemy_1)


if __name__ == '__main__':
    bc.connect()
    game = bc.game_scene()

    while True:
        _click_research_button(game)
